# vue-douban-webapp

## 项目介绍
用 vue + vue-router + vuex 写的，基于 vue-cli 的 webpack template 的仿豆瓣手机端网站的web app。

## 使用说明

``` bash
# 安装依赖
npm install

# 启动开发服务器，地址为 localhost:8080
npm run dev

# 生产环境打包发布
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
