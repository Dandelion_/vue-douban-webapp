import Vue from 'vue';
import Router from 'vue-router';
import HomeView from '../views/HomeView';
import MovieView from '../views/MovieView';

Vue.use(Router);

const router = new Router({
    routes: [{
            path: '/',
            name: 'Home',
            component: HomeView,
            meta: {
                title: '豆瓣'
            }
        },
        {
            path: '/movie',
            name: 'Movie',
            component: MovieView,
            meta: {
                title: '电影 - 豆瓣'
            }
        }
    ]
});

// 注册全局后置钩子
router.afterEach((to, from) => {
    // 当页面路由变化时修改title
    document.title = to.meta.title;
});

export default router;
