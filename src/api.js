/**
 * Created by superman on 2016/12/2.
 * https://github.com/superman66/node-proxy-api
 */
import axios from 'axios';

// 使用代理
// const HOST = process.env.NODE_ENV === 'production' ? 'https://node-douban-api.herokuapp.com' : 'http://localhost:8084';
const HOST = 'https://node-douban-api.herokuapp.com';

function fetch(url) {
    return new Promise((resolve, reject) => {
        axios.get(HOST + url)
            .then(response => {
                resolve(response.data);
            })
            .catch(error => {
                reject(error);
            });
    });
}

/**
 * 获取正在上映数据
 */
function fetchShowingMovies(count = 8) {
    return fetch(`/movie/in_theaters?count=${count}`);
}

/**
 * 获取即将上映数据
 */
function fetchLatestMovies(count = 8) {
    return fetch(`/movie/coming_soon?count=${count}`);
}

/**
 * 获取豆瓣top250数据
 */
function fetchDoubanTopMovies(count = 8) {
    return fetch(`/movie/top250?count=${count}`);
}

export default {
    fetch,
    fetchShowingMovies,
    fetchDoubanTopMovies,
    fetchLatestMovies
};
